<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<!--meta tags-->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="description" content="<?php if ( is_single() ) {
        single_post_title('', true); 
    } else {
        bloginfo('name'); echo " - "; bloginfo('description');
    }
    ?>" />
<title><?php
	wp_title( '|', true, 'right' );
	// Add the site's name.
	bloginfo( 'name' );
	// Add the site's description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	?></title>
<!--style tags-->
<link href="<?php bloginfo( 'stylesheet_url' ); ?>" rel="stylesheet" media="only screen" />
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.ico" type="image/x-icon" />	
<link rel="apple-touch-icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/img/medallion.png"/>
 <!--[if lt IE 9]>
  	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]--> 
<!-- wordpress auto tags -->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="container">
  <header>
  <h1><a href="<?php bloginfo('url'); ?>"><img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" title="<?php bloginfo('name'); ?>" alt="<?php bloginfo('name'); ?>" /></a></h1>
  <nav>
  
  	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('pages')) : else : ?>
    	<ul>
  	<?php wp_list_pages('title_li='); ?> 
  		</ul>
	<?php endif; ?>
  	
  </nav>
  <div>
 <dl>
  	<dt>email</dt>
    	<dd><?php  
    		/* at engage site this bookmark number is hardcoded as 8 */
	    	$bookmark = get_bookmark(8);
	    	echo '<a href="'.$bookmark->link_url.'">'.$bookmark->link_name.'</a>';
	    ?></dd>
    <dt>telephone</dt>
   	 	<dd class="number"><?php /* at engage site this bookmark number is hardcoded as 10 */
   	 	echo get_bookmark(10)->link_name; ?></dd>
  </dl>
  </div>
  <div class="social">
	  <?php  
	  		/* Spotify link and image
	  		 at engage site this bookmark number is hardcoded as 13 */
	    	$bookmark = get_bookmark(13);
	    	echo '<a href="'.$bookmark->link_url.'"><img src="'.$bookmark->link_image.'" alt="Spotify" /></a>';
	    ?>
  </div>
  <div class="social">
	  <?php  
	  		/* Twitter link and image
	  		 at engage site this bookmark number is hardcoded as 27 */
	    	$bookmark = get_bookmark(27);
	    	echo '<a href="'.$bookmark->link_url.'"><img src="'.$bookmark->link_image.'" alt="Twitter" /></a>';
	    ?>
  </div>
  <div class="social">
  <?php  
  			/* Facebook link and image
	  		 at engage site this bookmark number is hardcoded as 12 */	    	
	  		$bookmark = get_bookmark(12);
	    	echo '<a href="'.$bookmark->link_url.'"><img src="'.$bookmark->link_image.'" alt="Facebook" /></a>';
  ?>
  </div>
  </header>
<!-- finish header -->