<?php get_header(); ?>
<?php get_sidebar(); ?>
  <section>
<!-- The Loop begins -->
  	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article <?php post_class(); ?>>
    	<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
    	<?php the_content(); ?>
    </article>
    	<?php endwhile; else: ?>
    	<p>No content matches your criteria.</p>
    	<p><?php get_search_form(); ?></p>
    	<?php endif; ?>
<!-- The Loop ends. --> 
  </section>
<?php get_footer(); ?>