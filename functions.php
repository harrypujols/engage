<?php

//register jquery and custom scripts
wp_enqueue_script('scripts', get_bloginfo('stylesheet_directory').'/js/scripts.js', array('jquery'), time());

//register top menu
if ( function_exists('register_sidebar') )
register_sidebar(array(
	'name'=>'pages',
));

//register sidebar
if ( function_exists('register_sidebar') )
register_sidebar(array(
	'name'          => 'sidebar',
	'before_widget' => '<ul id="%1$s" class="side-nav widget %2$s">',
	'after_widget'  => '</ul>',
	'before_title'  => '<h2>',
	'after_title'   => '</h2>'
));

//custom login image
function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { 
        background-image:url('.get_bloginfo('template_directory').'/img/medallion.png) !important;
        background-size: 170px 170px !important; 
        width: 320px !important; 
        height: 170px !important;  
        }
    </style>';
}

add_action('login_head', 'my_custom_login_logo');

//custom post formats
add_theme_support( 'post-formats', array( 
	'image',  
	'video'
	));
	
//custom logo
$defaults = array(
	'default-image'          => get_template_directory_uri() . '/img/logo.gif',
	'random-default'         => false,
	'width'                  => 95,
	'height'                 => 113,
	'flex-height'            => false,
	'flex-width'             => false,
	'default-text-color'     => '',
	'header-text'            => false,
	'uploads'                => true,
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $defaults );

//adds title attribute to the menu
add_filter('walker_nav_menu_start_el', 'description_in_nav_el', 10, 4);
function description_in_nav_el($item_output, $item, $depth, $args)
{
	return preg_replace('/(<a.*?>[^<]*?)</', '$1' . "<", $item_output . "{$item->attr_title}");
}


?>