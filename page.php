<?php get_header(); ?>
<?php get_sidebar(); ?>
  <section>
<!-- The Loop begins -->
  	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article <?php post_class(); ?>>
    	<?php the_content(); ?>
    </article>
    	<?php endwhile; else: ?>
    	<p>We apologize. There's no content matching your criteria.</p>
    	<?php endif; ?>
<!-- The Loop ends. --> 
  </section>
<?php get_footer(); ?>