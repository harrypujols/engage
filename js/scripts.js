/* ===================================================
 * Les Engage Collective hackz
 * ===================================================
 * Prepared by Monsieur Harry Pujols
 * ========================================================== */
 
!function ($) {
  $(function () {

	  //remove the a tags from the current page item
	  $('.current_page_item').replaceWith( '<li class="current_page_item">' + $('.current_page_item').text() + '</li>' );

	  //add rollover class for the images on the work page
	  $('#work article a').addClass('rollover');

	  //remove the widget title
	  $('.widgettitle').remove();
        
	  //end	
  })
}(window.jQuery);